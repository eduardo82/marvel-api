package br.com.ciandt;

import java.util.Date;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.ciandt.config.ApplicationProperties;
import br.com.ciandt.constants.MarvelApiConstants;
import br.com.ciandt.model.MarvelCharacter;
import br.com.ciandt.util.ApiUtils;

public class CharacterServiceTest {
	private String basePath = "v1/public/characters";
	private String privateKey = "9eb31f59ddf700c8bc8109bb1207d79bfccfea98";
	private String apiKey = "06064cb0339c772ba33fb5b103473a70";
	private Long timeStamp;
	private String hash;

	@Autowired
	private ApplicationProperties properties;

	@Before
	public void setup() {
		timeStamp = new Date().getTime();
		hash = ApiUtils.generateHash(timeStamp + privateKey + apiKey);
	}

	@Test
	public void findAllMarvelCharacters() {
		Response response = ApiUtils.getRequest(ApiUtils.getWebTarget(basePath), hash, apiKey, timeStamp);
		MarvelCharacter[] personagens = (MarvelCharacter[]) ApiUtils.getMarvel(response, MarvelCharacter[].class);
		Assert.assertTrue(response.getStatus() == 200 && personagens.length > 1);
	}

	@Test
	public void findMarvelCharacter() {
		final String character = "hulk";
		WebTarget target = ApiUtils.getWebTarget(basePath).queryParam(MarvelApiConstants.NAME, character);
		Response response = ApiUtils.getRequest(target, hash, apiKey, timeStamp);
		String responseJson = response.readEntity(String.class);
		Assert.assertTrue(responseJson.contains(character));
	}

	@Test
	public void findComicMarvelCharacter() {
		final String idCharacter = "1016823";
		final String titleComic = "Ultimate X-Men Vol. VI: Return of the King (Trade Paperback)";
		Response response = ApiUtils.getRequest(ApiUtils.getWebTarget(basePath, idCharacter, MarvelApiConstants.COMICS),
				hash, apiKey, timeStamp);

		String responseJson = response.readEntity(String.class);
		Assert.assertTrue(responseJson.contains(titleComic));
	}
}
