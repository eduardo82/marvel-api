/**
 *
 */
package br.com.ciandt.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * @author eduardo.vasconcelos
 *
 */
@Component
@PropertySource("classpath:application.properties")
public class ApplicationProperties {


	@Value("${private-key}")
    private String privateKey;

	@Value("${api-key}")
    private String apiKey;

	@Value("${path-characters}")
	private String pathCharacter;

	@Value("${path-comics}")
	private String pathComic;

	public String getPrivateKey() {
		return privateKey;
	}
	public void setPrivateKey(String privateKey) {
		this.privateKey = privateKey;
	}
	public String getApiKey() {
		return apiKey;
	}
	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}
	public String getPathCharacter() {
		return pathCharacter;
	}
	public void setPathCharacter(String pathCharacter) {
		this.pathCharacter = pathCharacter;
	}
	public String getPathComic() {
		return pathComic;
	}
	public void setPathComic(String pathComic) {
		this.pathComic = pathComic;
	}
}
