/**
 *
 */
package br.com.ciandt.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.ciandt.model.Comic;

/**
 * @author eduardo.vasconcelos
 *
 */
public interface ComicRepository extends CrudRepository<Comic, Integer> {

}
