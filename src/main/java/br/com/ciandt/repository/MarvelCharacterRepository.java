package br.com.ciandt.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.ciandt.model.MarvelCharacter;

/**
 * @author eduardo.vasconcelos
 *
 */
public interface MarvelCharacterRepository extends CrudRepository<MarvelCharacter, Integer> {

}
