/**
 *
 */
package br.com.ciandt.constants;

/**
 * @author eduardo.vasconcelos
 *
 */
public interface MarvelApiConstants {

	public static final String API_KEY = "apikey";
	public static final String CHARACTERS = "characters";
	public static final String COMICS = "comics";
	public static final String DATA = "data";
	public static final String HASH = "hash";
	public static final String ID = "id";
	public static final String LIMIT = "limit";
	public static final String NAME = "name";
	public static final String NAME_STARTS_WITH = "nameStartsWith";
	public static final String NUMBER_LIMIT = "20";
	public static final String NUMBER_OFFSET = "10";
	public static final String OFFSET = "offset";
	public static final String ORDER_BY = "orderBy";
	public static final String SEPARATOR = "/";
	public static final String RESULTS = "results";
	public static final String TS = "ts";
	public static final String URL = "http://gateway.marvel.com";
}
