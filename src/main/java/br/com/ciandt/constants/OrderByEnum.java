/**
 *
 */
package br.com.ciandt.constants;

import java.util.ArrayList;
import java.util.List;

/**
 * @author eduardo.vasconcelos
 *
 */
public enum OrderByEnum {

	NAME_ASC("name"),
	MODIFIED_ASC("modified"),
	NAME_DESC("-name"),
	MODIFIED_DESC("-modified");

	private String value;

	private OrderByEnum(String value) {
		this.value = value;
	}

	public static List<String> getListValues() {
		List<String> values = new ArrayList<String>();
		for (OrderByEnum order : OrderByEnum.values()) {
			values.add(order.getValue());
		}
		return values;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}


}
