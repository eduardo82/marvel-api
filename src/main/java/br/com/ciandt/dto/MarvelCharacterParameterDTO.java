package br.com.ciandt.dto;

public class MarvelCharacterParameterDTO {

	private String name;
	private String nameStartsWith;
	private String orderBy;
	private Integer limit;
	private Integer offset;
	private String comics;

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNameStartsWith() {
		return nameStartsWith;
	}
	public void setNameStartsWith(String nameStartsWith) {
		this.nameStartsWith = nameStartsWith;
	}
	public Integer getLimit() {
		return limit;
	}
	public void setLimit(Integer limit) {
		this.limit = limit;
	}
	public Integer getOffset() {
		return offset;
	}
	public void setOffset(Integer offset) {
		this.offset = offset;
	}
	public String getOrderBy() {
		return orderBy;
	}
	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}
	public String getComics() {
		return comics;
	}
	public void setComics(String comics) {
		this.comics = comics;
	}
}
