package br.com.ciandt.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ciandt.model.MarvelCharacter;
import br.com.ciandt.repository.MarvelCharacterRepository;

@Service
public class MarvelCharacterService {

	@Autowired
	private MarvelCharacterRepository repository;

	public Iterable<MarvelCharacter> findAll() {
		return repository.findAll();
	}

	public void save(List<MarvelCharacter> personagens) {
		repository.save(personagens);
	}

	public void save(MarvelCharacter personagens) {
		repository.save(personagens);
	}

	public void delete(Integer id) {
		repository.delete(id);
	}

	public MarvelCharacter findOne(Integer id) {
		return repository.findOne(id);
	}

}
