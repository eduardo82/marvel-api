package br.com.ciandt.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.ciandt.model.Comic;
import br.com.ciandt.repository.ComicRepository;

@Service
@Transactional
public class ComicService {

	@Autowired
	private ComicRepository repository;

	public Iterable<Comic> findAll() {
		return repository.findAll();
	}

	public void save(List<Comic> personagens) {
		repository.save(personagens);
	}

	public void save(Comic personagem) {
		repository.save(personagem);
	}

	public void delete(Integer id) {
		repository.delete(id);
	}

	public Comic findOne(Integer id) {
		return repository.findOne(id);
	}

}
