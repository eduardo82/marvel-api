package br.com.ciandt.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import br.com.ciandt.config.ValidationProperties;
import br.com.ciandt.model.MarvelCharacter;

@Component
public class CharacterValidator implements Validator {

	@Autowired
	private ValidationProperties properties;

	@Override
	public boolean supports(Class<?> clazz) {
		return MarvelCharacter.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		MarvelCharacter character = (MarvelCharacter) target;

		if (StringUtils.isEmpty(character.getName())) {
			errors.reject("name", properties.getName());
		}

		errors.addAllErrors(errors);

	}


}
