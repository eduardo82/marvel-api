/**
 *
 */
package br.com.ciandt.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import br.com.ciandt.config.ValidationProperties;
import br.com.ciandt.model.Comic;

/**
 * @author eduardo.vasconcelos
 *
 */
@Component
public class ComicValidator implements Validator {

	@Autowired
	private ValidationProperties properties;

	@Override
	public boolean supports(Class<?> clazz) {
		return Comic.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		Comic comic = (Comic) target;

		if (StringUtils.isEmpty(comic.getTitle())) {
			errors.reject("title", properties.getTitle());
		}

		errors.addAllErrors(errors);

	}

}
