package br.com.ciandt.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="character")
public class MarvelCharacter implements IMarvel {

	private static final long serialVersionUID = -5191359577243456643L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;

	@Column(nullable = false)
	private String name;

	@Column(length=1000)
	private String description;

	@Column
	private String resourceURI;

	@OneToOne(cascade=CascadeType.ALL)
	private Thumbnail thumbnail;

	@OneToOne(cascade=CascadeType.ALL)
	private ComicList comics;

	@OneToOne(cascade=CascadeType.ALL)
	private SerieList series;

	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	private List<Url> urls;

	@Transient
	private List<Comic> comicsCharacter;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getResourceURI() {
		return resourceURI;
	}
	public void setResourceURI(String resourceURI) {
		this.resourceURI = resourceURI;
	}
	public Thumbnail getThumbnail() {
		return thumbnail;
	}
	public void setThumbnail(Thumbnail thumbnail) {
		this.thumbnail = thumbnail;
	}
	public ComicList getComics() {
		return comics;
	}
	public void setComics(ComicList comics) {
		this.comics = comics;
	}
	public List<Url> getUrls() {
		return urls;
	}
	public void setUrls(List<Url> urls) {
		this.urls = urls;
	}
	public SerieList getSeries() {
		return series;
	}
	public void setSeries(SerieList series) {
		this.series = series;
	}

	public List<Comic> getComicsCharacter() {
		return comicsCharacter;
	}
	public void setComicsCharacter(List<Comic> comicsCharacter) {
		this.comicsCharacter = comicsCharacter;
	}
}
