/**
 *
 */
package br.com.ciandt.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * @author eduardo.vasconcelos
 *
 */
@Entity
@Table(name="comic")
public class Comic implements IMarvel {

	private static final long serialVersionUID = 4703826982937613026L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;

	@Column
	private String title;

	@Column(length=1500)
	private String description;

	@Column
	private Date modified;

	@Column
	private String isbn;

	@Column
	private String upc;

	@Column
	private String diamondCode;

	@Column
	private Integer digitalId;

	@Column
	private String ean;

	@Column
	private String issn;

	@Column
	private String format;

	@Column
	private int pageCount;

	@Column
	private String resourceURI;

	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	private List<Url> urls;

	@OneToOne(cascade=CascadeType.ALL)
	private Thumbnail thumbnail;

	@OneToOne(cascade=CascadeType.ALL)
	private Image image;

	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	private List<ComicPrice> prices;

	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	private List<ComicDate> dates;


	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getModified() {
		return modified;
	}
	public void setModified(Date modified) {
		this.modified = modified;
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public String getUpc() {
		return upc;
	}
	public void setUpc(String upc) {
		this.upc = upc;
	}
	public String getDiamondCode() {
		return diamondCode;
	}
	public void setDiamondCode(String diamondCode) {
		this.diamondCode = diamondCode;
	}
	public String getEan() {
		return ean;
	}
	public void setEan(String ean) {
		this.ean = ean;
	}
	public String getIssn() {
		return issn;
	}
	public void setIssn(String issn) {
		this.issn = issn;
	}
	public String getFormat() {
		return format;
	}
	public void setFormat(String format) {
		this.format = format;
	}
	public int getPageCount() {
		return pageCount;
	}
	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}
	public String getResourceURI() {
		return resourceURI;
	}
	public void setResourceURI(String resourceURI) {
		this.resourceURI = resourceURI;
	}
	public List<Url> getUrls() {
		return urls;
	}
	public void setUrls(List<Url> urls) {
		this.urls = urls;
	}
	public Thumbnail getThumbnail() {
		return thumbnail;
	}
	public void setThumbnail(Thumbnail thumbnail) {
		this.thumbnail = thumbnail;
	}
	public Integer getDigitalId() {
		return digitalId;
	}
	public void setDigitalId(Integer digitalId) {
		this.digitalId = digitalId;
	}
	public Image getImage() {
		return image;
	}
	public void setImage(Image image) {
		this.image = image;
	}
	public List<ComicPrice> getPrices() {
		return prices;
	}
	public void setPrices(List<ComicPrice> prices) {
		this.prices = prices;
	}
	public List<ComicDate> getDates() {
		return dates;
	}
	public void setDates(List<ComicDate> dates) {
		this.dates = dates;
	}
}