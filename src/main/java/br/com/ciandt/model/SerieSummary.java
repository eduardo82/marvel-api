/**
 *
 */
package br.com.ciandt.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author eduardo.vasconcelos
 *
 */
@Entity
@Table(name="serie_summary")
public class SerieSummary implements Serializable {

	private static final long serialVersionUID = 5294153185411196255L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;

	@Column(name="resource")
	private String resourceURI;

	@Column(name="name")
	private String name;

	@Column(name="type")
	private String type;

	public String getResourceURI() {
		return resourceURI;
	}
	public void setResourceURI(String resourceURI) {
		this.resourceURI = resourceURI;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
}
