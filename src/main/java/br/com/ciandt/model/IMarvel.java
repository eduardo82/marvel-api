/**
 *
 */
package br.com.ciandt.model;

import java.io.Serializable;

/**
 * @author eduardo.vasconcelos
 *
 */
public interface IMarvel extends Serializable {

	public Integer getId();

	public void setId(Integer id);
}
