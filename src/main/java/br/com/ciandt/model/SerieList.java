/**
 *
 */
package br.com.ciandt.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author eduardo.vasconcelos
 *
 */
@Entity
@Table(name="serie")
public class SerieList implements Serializable {

	private static final long serialVersionUID = 6603798885471559749L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;

	@Column(name="avaliable")
	private Integer avaliable;

	@Column(name="collection")
	private String collectionURI;

	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	private List<SerieSummary> items;

	@Column(name="returned")
	private Integer returned;

	public Integer getAvaliable() {
		return avaliable;
	}
	public void setAvaliable(Integer avaliable) {
		this.avaliable = avaliable;
	}
	public String getCollectionURI() {
		return collectionURI;
	}
	public void setCollectionURI(String collectionURI) {
		this.collectionURI = collectionURI;
	}
	public List<SerieSummary> getItems() {
		return items;
	}
	public void setItems(List<SerieSummary> items) {
		this.items = items;
	}
	public Integer getReturned() {
		return returned;
	}
	public void setReturned(Integer returned) {
		this.returned = returned;
	}



}
