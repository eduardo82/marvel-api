package br.com.ciandt.util;

import java.lang.reflect.Type;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import br.com.ciandt.constants.MarvelApiConstants;
import br.com.ciandt.model.IMarvel;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * Classe utilitária para a API.
 * @author eduardo.vasconcelos
 *
 */
public class ApiUtils {

	/**
	 * Converte a string para um hash e retorna a string gerada a partir do hash.
	 * @param parametro a ser utilizado.
	 * @return string gerada a partir do parametro.
	 */
	public static String generateHash(String parametro) {
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		md.update(parametro.getBytes());
		byte[] hashGerado = md.digest();

		StringBuilder s = new StringBuilder();
		for (int i = 0; i < hashGerado.length; i++) {
			int parteAlta = ((hashGerado[i] >> 4) & 0xf) << 4;
			int parteBaixa = hashGerado[i] & 0xf;
			if (parteAlta == 0) {
				s.append('0');
			}
			s.append(Integer.toHexString(parteAlta | parteBaixa));
		}
		return s.toString();
	}

	public static WebTarget getWebTarget(String... paths) {
		Client client = ClientBuilder.newClient();
		WebTarget target = client.target(MarvelApiConstants.URL);

		if (paths != null && paths.length > 0) {
			for (String path : paths) {
				target = target.path(path);
			}
		}

		return target;
	}

	public static Response getRequest(WebTarget webTaget, String hash, String apiKey, Long timeStamp) {
		return webTaget.queryParam(MarvelApiConstants.API_KEY, apiKey)
					.queryParam(MarvelApiConstants.TS, timeStamp)
					.queryParam(MarvelApiConstants.HASH, hash)
					.request().get();
	}


	public static IMarvel[] getMarvel(Response response, Type clazz) {

		String responseJson = response.readEntity(String.class);
		JsonObject json = (JsonObject) new JsonParser().parse(responseJson);
		JsonObject dado = (JsonObject) json.get(MarvelApiConstants.DATA);
		JsonArray arrayJson = (JsonArray) dado.get(MarvelApiConstants.RESULTS);

		Gson gson =  new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create();
		return gson.fromJson(arrayJson, clazz);
	}
}
