package br.com.ciandt.dojo.controller;

import java.util.Arrays;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.com.ciandt.config.ApplicationProperties;
import br.com.ciandt.constants.MarvelApiConstants;
import br.com.ciandt.constants.OrderByEnum;
import br.com.ciandt.dto.MarvelCharacterParameterDTO;
import br.com.ciandt.model.Comic;
import br.com.ciandt.model.MarvelCharacter;
import br.com.ciandt.service.ComicService;
import br.com.ciandt.service.MarvelCharacterService;
import br.com.ciandt.util.ApiUtils;

@Controller("/")
public class HomeController {

	private static final String ORDER_LIST = "ordersList";
	private static final String MARVEL_CHARACTER = "marvelCharacter";
	private static final String CHARACTERS = "characters";

	@Autowired
	private ApplicationProperties properties;

	@Autowired
	private MarvelCharacterService characterService;

	@Autowired
	private ComicService comicService;

	private Long timeStamp;
	private String hash;
	private String basePath;
	private WebTarget target;

	@PostConstruct
	public void loadValues() {
		timeStamp = new Date().getTime();
		hash = ApiUtils.generateHash(timeStamp + properties.getPrivateKey() + properties.getApiKey());
		basePath = properties.getPathCharacter();
	}

	@RequestMapping(method = RequestMethod.GET)
    public ModelAndView index(Model model) {
		model.addAttribute(MARVEL_CHARACTER, new MarvelCharacterParameterDTO());
		model.addAttribute(ORDER_LIST, OrderByEnum.getListValues());

        return new ModelAndView("/index");
    }


	@RequestMapping(value = CHARACTERS, method = RequestMethod.POST)
    public ModelAndView personagens(MarvelCharacterParameterDTO characther, Model model) {
		target = ApiUtils.getWebTarget(basePath)
				.queryParam(MarvelApiConstants.LIMIT, characther.getLimit())
				.queryParam(MarvelApiConstants.OFFSET, characther.getOffset())
				.queryParam(MarvelApiConstants.API_KEY, properties.getApiKey())
				.queryParam(MarvelApiConstants.TS, timeStamp)
				.queryParam(MarvelApiConstants.HASH, hash);

		if (!StringUtils.isEmpty(characther.getNameStartsWith())) {
			target = target.queryParam(MarvelApiConstants.NAME_STARTS_WITH, characther.getNameStartsWith());
		}

		if (!StringUtils.isEmpty(characther.getName())) {
			target = target.queryParam(MarvelApiConstants.NAME_STARTS_WITH, characther.getName());
		}

		if (!StringUtils.isEmpty(characther.getOrderBy())) {
			target = target.queryParam(MarvelApiConstants.ORDER_BY, characther.getOrderBy());
		}

		if (!StringUtils.isEmpty(characther.getComics())) {
			target = target.queryParam(MarvelApiConstants.COMICS, characther.getComics());
		}


		Response response = target.request().get();

		MarvelCharacter[] personagens = (MarvelCharacter[]) ApiUtils.getMarvel(response, MarvelCharacter[].class);

		for (MarvelCharacter personagem : personagens) {
			if (personagem.getComics().getReturned() > 0) {
				//Fixo em 1 para não onerar a chamada.
				target = ApiUtils.getWebTarget(basePath, String.valueOf(personagem.getId()), MarvelApiConstants.COMICS)
						.queryParam(MarvelApiConstants.LIMIT, 1)
						.queryParam(MarvelApiConstants.OFFSET, 1)
						.queryParam(MarvelApiConstants.API_KEY, properties.getApiKey())
						.queryParam(MarvelApiConstants.TS, timeStamp)
						.queryParam(MarvelApiConstants.HASH, hash);

				response = target.request().get();
				Comic[] comics = (Comic[]) ApiUtils.getMarvel(response, Comic[].class);

				comicService.save(Arrays.asList(comics));

				personagem.setComicsCharacter(Arrays.asList(comics));
			}
		}

		characterService.save(Arrays.asList(personagens));

		model.addAttribute(CHARACTERS, Arrays.asList(personagens));

		return new ModelAndView(CHARACTERS);
    }

	public WebTarget getTarget() {
		return target;
	}

	public void setTarget(WebTarget target) {
		this.target = target;
	}
}
