/**
 *
 */
package br.com.ciandt.dojo.controller;

import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.ciandt.model.MarvelCharacter;
import br.com.ciandt.service.MarvelCharacterService;
import br.com.ciandt.validator.CharacterValidator;

import com.google.gson.Gson;

/**
 * @author eduardo.vasconcelos
 *
 */
@RestController
@RequestMapping("/rest_characters")
public class MarvelCharacterController {

	@Autowired
	private MarvelCharacterService service;

	@Autowired
	private CharacterValidator validator;

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON )
	public ResponseEntity<String> comics() {
		try {
			String characters = new Gson().toJson(service.findAll());
			ResponseEntity<String> response = new ResponseEntity<String>(characters, HttpStatus.OK);
			return response;

		} catch (Exception e) {
			return new ResponseEntity<String>(HttpStatus.BAD_REQUEST.getReasonPhrase(), HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Void> create(@RequestBody MarvelCharacter character,
			BindingResult result, Model model ) {
		try {
			validator.validate(character, result);

			if (!result.hasErrors()) {
				service.save(character);
				return ResponseEntity.ok().build();
			}
			else {
				throw new Exception();
			}

		} catch (Exception e) {
			return ResponseEntity.badRequest().build();
		}
	}

	@RequestMapping(value="/{id}", method = RequestMethod.PUT )
	public ResponseEntity<Void> update(@PathVariable("id") Integer id, @RequestBody MarvelCharacter character,
			BindingResult result, Model model) {
		try {
			if (!result.hasErrors()) {
				MarvelCharacter marvelCharacter = service.findOne(id);
				if (marvelCharacter == null) {
					return ResponseEntity.notFound().build();
				}
				marvelCharacter.setName(character.getName());
				marvelCharacter.setDescription(character.getDescription());
				marvelCharacter.setResourceURI(character.getResourceURI());
				marvelCharacter.setComics(character.getComics());

				service.save(marvelCharacter);
				return ResponseEntity.ok().build();
			}
			else {
				throw new Exception();
			}
		} catch (Exception e) {
			return ResponseEntity.badRequest().build();
		}
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON )
	public ResponseEntity<String> findOne(@PathVariable int id) {
		try {
			String character = new Gson().toJson(service.findOne(id));
			ResponseEntity<String> response = new ResponseEntity<String>(character, HttpStatus.OK);
			return response;

		} catch (Exception e) {
			return new ResponseEntity<String>(HttpStatus.BAD_REQUEST.getReasonPhrase(), HttpStatus.BAD_REQUEST);
		}
	}


	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON )
	public ResponseEntity<Void> delete(@PathVariable int id) {
		try {
			service.delete(id);
			return ResponseEntity.ok().build();
		} catch (Exception e) {
			return ResponseEntity.badRequest().build();
		}

	}


}