/**
 *
 */
package br.com.ciandt.dojo.controller;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.ciandt.model.Comic;
import br.com.ciandt.service.ComicService;
import br.com.ciandt.validator.ComicValidator;

import com.google.gson.Gson;

/**
 * @author eduardo.vasconcelos
 *
 */
@RestController
@RequestMapping("/rest_comics")
public class MarvelComicsController {

	@Autowired
	private ComicValidator validator;

	@Autowired
	private ComicService service;

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON )
	public ResponseEntity<String> comics() {
		try {
			String comics = new Gson().toJson(service.findAll());
			ResponseEntity<String> response = new ResponseEntity<String>(comics, HttpStatus.OK);
			return response;

		} catch (Exception e) {
			return new ResponseEntity<String>(HttpStatus.BAD_REQUEST.getReasonPhrase(), HttpStatus.BAD_REQUEST);
		}
	}

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Void> create(@RequestBody Comic comic,
			BindingResult result, Model model) {
		try {
			if (!result.hasErrors()) {
				service.save(comic);
				return ResponseEntity.ok().build();
			}
			else {
				throw new Exception();
			}

		} catch (Exception e) {
			return ResponseEntity.badRequest().build();
		}
	}

	@RequestMapping(value="/{id}" , method = RequestMethod.PUT)
	public ResponseEntity<Void> update(@PathVariable Integer id, @RequestBody Comic comic,
			BindingResult result, Model model) {
		try {
			if (!result.hasErrors()) {
				Comic marvelComic = service.findOne(id);
				if (marvelComic == null) {
					return ResponseEntity.notFound().build();
				}
				marvelComic.setTitle(comic.getTitle());
				marvelComic.setDescription(comic.getDescription());
				marvelComic.setResourceURI(comic.getResourceURI());
				marvelComic.setUrls(comic.getUrls());


				service.save(marvelComic);
				return ResponseEntity.ok().build();
			}
			else {
				throw new Exception();
			}

		} catch (Exception e) {
			return ResponseEntity.badRequest().build();
		}
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON )
	public ResponseEntity<String> findOne(@PathVariable Integer id) {
		try {
			String comics = new Gson().toJson(service.findOne(id));
			ResponseEntity<String> response = new ResponseEntity<String>(comics, HttpStatus.OK);
			return response;

		} catch (Exception e) {
			return new ResponseEntity<String>(HttpStatus.BAD_REQUEST.getReasonPhrase(), HttpStatus.BAD_REQUEST);
		}
	}


	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON )
	public ResponseEntity<Void> delete(@PathVariable Integer id) {
		try {
			service.delete(id);
			return ResponseEntity.ok().build();
		} catch (Exception e) {
			return ResponseEntity.badRequest().build();
		}

	}
}
