<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<html>
	<head>
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" />
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

		<title>Marvel API</title>
		<style type="text/css">
			fieldset.parameter-border {
			    border: 1px groove #ddd !important;
			    padding: 0 1.4em 1.4em 1.4em !important;
			    margin: 0 0 1.5em 0 !important;
			    -webkit-box-shadow:  0px 0px 0px 0px #000;
			            box-shadow:  0px 0px 0px 0px #000;
			}

			legend.parameter-border {
			    font-size: 1.2em !important;
			    font-weight: bold !important;
			    text-align: left !important;
			    width:auto;
			    padding:0 10px;
			    border-bottom:none;
			}

			.center-label {
			   	padding: 6px 0px;
			}
		</style>
	</head>
	<body>
		<form:form action="characters" method="post" modelAttribute="marvelCharacter" >
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12">
						<fieldset class="parameter-border">
						    <legend class="parameter-border">Busca Personagens Marvel</legend>
						    <div class="row">
						    	<div class="col-sm-1">
							        <label id="labelNome" class="control-label input-label pull-right center-label" for="name">Nome:</label>
						    	</div>
						    	<div class="col-sm-2">
									<form:input id="name" path="name" cssClass="form-control " />
						    	</div>

						    	<div class="col-sm-2">
							        <label id="labelNameStartsWith" class="control-label input-label pull-right center-label" for="nameStartsWith">Nome Começado Com:</label>
						    	</div>
						    	<div class="col-sm-2">
									<form:input id="nameStartsWith" path="nameStartsWith" cssClass="form-control" />
						    	</div>
						    	<div class="col-sm-1">
							        <label class="control-label input-label pull-right center-label" for="orderBy">Ordenado Por:</label>
						    	</div>
						    	<div class="col-sm-2">
									<form:select id="orderBy" path="orderBy" cssClass="form-control" multiple="true">
					  					<form:options items="${ordersList}"  />
									</form:select>
						    	</div>
						    </div>
						    <div class="row" style="padding-top:10px">
						    	<div class="col-sm-1">
							        <label class="control-label input-label pull-right center-label" for="revistas">Revistas:</label>
						    	</div>
						    	<div class="col-sm-2">
						    		<form:input id="comics" path="comics" cssClass="form-control" title="Aceita lista de ids separados por vírgulas" />
						    	</div>
						    	<div class="col-sm-2">
							        <label class="control-label input-label pull-right center-label" for="limit">Limite:</label>
						    	</div>
						    	<div class="col-sm-1">
									<form:select id="limit" path="limit" cssClass="form-control">
					  					<form:option value="1" label="1" />
					  					<form:option value="3" label="3" />
					  					<form:option value="5" label="5" />
					  					<form:option value="10" label="10" />
					  					<form:option value="20" label="20" />
					  					<form:option value="50" label="50" />
					  					<form:option value="100" label="100" />
									</form:select>
						    	</div>

						    	<div class="col-sm-2">
							        <label class="control-label input-label pull-right center-label" for="offset">Quantidade:</label>
						    	</div>
						    	<div class="col-sm-1">
									<form:select id="offset" path="offset" cssClass="form-control">
					  					<form:option value="1" label="1" />
					  					<form:option value="3" label="3" />
					  					<form:option value="5" label="5" />
					  					<form:option value="10" label="10" />
					  					<form:option value="20" label="20" />
					  					<form:option value="50" label="50" />
					  					<form:option value="100" label="100" />
									</form:select>
						    	</div>
						    </div>
						    <div class="row" style="padding-top:10px">
						    	<div class="col-12">
									<button id="btnPesquisar" class="btn btn-primary pull-right" style="width:120px">
										Pesquisar
									</button>
						    	</div>
						    </div>
						</fieldset>
					</div>
				</div>
			</div>
		</form:form>

	</body>
</html>