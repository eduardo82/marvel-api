<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<html>
	<head>
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" />
		<link href="/resources/css/page.css" rel="stylesheet" type="text/css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Personagens</title>

		<style type="text/css">
			fieldset.parameter-border {
			    border: 1px groove #ddd !important;
			    padding: 0 1.4em 1.4em 1.4em !important;
			    margin: 0 0 1.5em 0 !important;
			    -webkit-box-shadow:  0px 0px 0px 0px #000;
			            box-shadow:  0px 0px 0px 0px #000;
			}

			legend.parameter-border {
			    font-size: 1.2em !important;
			    font-weight: bold !important;
			    text-align: left !important;
			    width:auto;
			    padding:0 10px;
			    border-bottom:none;
			}

		</style>

		<script>
		    $(document).ready(function() {
		    	$('.btn').click(function() {
					window.location.href = "${pageContext.request.contextPath}/";
		    	});
		   });
		</script>
	</head>
	<body>
		<div class="container-fluid">
			<div class="row" style="padding-top:10px">
				<div class="col-sm-12">
					<fieldset class="parameter-border">
					    <legend class="parameter-border">Personagens</legend>
					    <div class="row" style="padding-bottom:10px">
							<div class="col-sm-12">
								<button id="btnVoltarTop" class="btn btn-primary pull-right" style="width:120px">
									Voltar
								</button>
							</div>
						</div>
						<div class="table-responsive table-bordered table-hover">
							<table class="table table-bordered">
							    <thead>
							      <tr>
							        <th width="100" style="text-align:center">#</th>
							        <th width="250"></th>
							        <th width="200">Nome</th>
							        <th width="500">Descrição</th>
							        <th width="200" style="text-align:center">Quantidade de Revistas</th>
							        <th width="200" style="text-align:center">Quantidade de Séries</th>
							        <th>Links Úteis</th>
							        <th>Revista (Exemplo)</th>
							      </tr>
							    </thead>
							    <tbody>
							    	<c:forEach items="${characters}" var="character" >
									      <tr>
									        <td style="text-align:center">${character.id}</td>
									        <td><img src='${character.thumbnail.path}.${character.thumbnail.extension}' alt="No Image" height="250" width="250"/></td>
									        <td>${character.name}</td>
									        <td>${character.description}</td>
									        <td style="text-align:center">${character.comics.returned}</td>
									        <td style="text-align:center">${character.series.returned}</td>

									        <td>
									        	<c:forEach items="${character.urls}" var="url" >
									        		<a href="${url.url}" target="_blank">${url.type}</a><br/>
									        	</c:forEach>
									        </td>

									        <td>
									        	<c:forEach items="${character.comicsCharacter}" var="comic" >
									        		<c:forEach items="${comic.urls}" var="url" >
										        		<a href="${url.url}" target="_blank">${url.type}</a><br/>
									        		</c:forEach>
									        	</c:forEach>
									        </td>
									      </tr>
							    	</c:forEach>
							    </tbody>
							  </table>
						</div>
						<div class="row" style="padding-top:10px">
							<div class="col-sm-12">
								<button id="btnVoltarDown" class="btn btn-primary pull-right" style="width:120px">
									Voltar
								</button>
							</div>
						</div>
					</fieldset>
				</div>
			</div>
		</div>

	</body>
</html>